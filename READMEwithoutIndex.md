本共享库采用GPLV3许可证，请在所有提交代码开头包括许可证，如下：
```
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

```
- 目标：共享一些常用代码块，以及示例代码。代码请遵循[规范](http://tieba.baidu.com/p/3818941022)。为方便索引生成，请在开头注释中说明开发环境和最佳编译运行环境、版本；程序整体用途，输入输出，备注等。范例：[TIME_DELAY.asm](http://git.oschina.net/zhishi/asm_for_all/blob/master/example/x86/console/TIME_DELAY.asm)。

- 库结构：分成example(示例)，与template(代码块)。分别再按照指令集(x86,arm...)与系统(console,win32,linux...)分两层，如下:
- example  
-- x86  
--- console  
--- win32  
--- linux（暂无)  
-- arm（暂无）  
- template  
-- x86  
--- console  
--- ...  

管理员责任：对不同用户共享的代码进行走查（code review），根据代码规范对开发者（包括其他管理员）进行修改建议。

根据代码开头的注释，生成代码索引如下。工具代码请见[项目管理工具/IndexGenerator](http://git.oschina.net/zhishi/asm_for_all/tree/master/项目管理工具/IndexGenerator).

