;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

;MASMPlus 代码模板 - 控制台程序

.386
.model flat, stdcall
option casemap :none

include windows.inc
include user32.inc
include kernel32.inc
include masm32.inc
include gdi32.inc

includelib gdi32.lib
includelib user32.lib
includelib kernel32.lib
includelib masm32.lib
include macro.asm
.data
    TRAS_HEX DWORD 01ACDBE0FH
	lpShowMsg1 BYTE "变量[TRAS_HEX]十六进制数:0x"
	lpHEXAdd BYTE 8 dup (?),0
	lpShowMsg2 BYTE ",存在于程序数据段内存地址:"
	lpAddAdd BYTE 8 dup (?),"H",0dh,0ah,0
.data?
	buffer	db MAX_PATH dup(?)
	
.CODE
START:
	mov eax,TRAS_HEX
	lea edi,lpHEXAdd
	add edi,6
	mov ecx,4
L1:push eax
   call HEX2ASCII
	mov WORD ptr [edi],ax
	sub edi,2
	pop eax
	shr eax,8
	loop L1
	lea eax,TRAS_HEX
	lea edi,lpAddAdd
	add edi,6
	mov ecx,4
L2:push eax
   call HEX2ASCII
   mov WORD ptr [edi],ax
   sub edi,2
   pop eax
   shr eax,8
   loop L2
	invoke StdOut,offset lpShowMsg1
	invoke StdOut,offset lpShowMsg2
	
	;暂停显示,回车键关闭
	invoke StdIn,addr buffer,sizeof buffer
	invoke ExitProcess,0
HEX2ASCII proc near
          push edx
          push eax
          shr al,4
          cmp al,9
          jbe next1
          add al,7
    next1:add al,030h
          mov dl,al
          pop eax
          and al,0fH
          cmp al,9
          jbe next2
          add al,7
    next2:add al,030h
          mov dh,al
          xor eax,eax
          mov eax,edx
          pop edx
          ret
HEX2ASCII endp
end START