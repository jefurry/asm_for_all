;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;程序名称:HEXVIEW.ASM
;功能:文件的十六进制观看器 HEX VIEWER
;环境:16BIT DOS实模式(windows/dos或dosbox)
;编译器:MASM 5.1 (这个是com格式，masm5.x须用EXE2BIN HEXVIEW.EXE HEXVIEW.COM，Masm6.x须加入：ml /ＡＴ参数
;用法:看说明
;返回值:没有
;破坏寄存器:不适用
;
;使用方法: HEXVIEW <Filename>
;程式会读入档案,并以十六进制显示,按上下pgup,pgdown,home,end观看,ESC离开
;为减少复杂度,最大读入档案大小512k,大于则只读入前512k部份
;
;注意1:这只是一个范例程式，具备最起码的功能，没有太花巧的技术.
;若你需要其他功能或加入或删减任何代码，请随便，但不能要求作者为你订制特定的功能或
;根据功课要求而改变这个子程序改变那个子程序，若是代码错误或优化代码的意见，
;则欢迎提出:)
;
;注意2:伸手党拿去取用没问题，但[不能]在代码任何位置或标题或程式上加上个人资料
;例如作者谁谁谁，或者Written by XXX等等，这代码是本人的烂创作，不是谁或谁写的东西
;本人都不加名字了！
;
;程序只加了一些简单解释，若深入理解程式运作，请自行用debug，不要问我。
;
;代码及程式;
;http://pan.baidu.com/s/1i3zUvvn
;
.286
esc_key         equ     01h ;定义按键
up_key          equ     48h
down_key        equ     50h
left_key        equ     4bh
right_key       equ     4dh
home_key        equ     47h
end_key         equ     4fh
pgup_key        equ     49h
pgdn_key        equ     51h
text_color      equ     1Bh ;定义颜色
offset_color    equ     1Eh
char_color      equ     1Ah
title1_color    equ     30h
title2_color     equ     34h ;30h
back_color      equ     17h
dos_max_seg     equ     8  ;最大段数 8 个 8 x 64 = 512k
space_colAtt    equ     1720h ;空白加颜色值 蓝底

code_seg        segment
                assume  cs:code_seg,ds:nothing,es:nothing
                org     100h  ;com 档格式
start:          jmp     begin

help_str        db      'Usage: HexV filename',10,13,'$'
error_str    db	10,13,'Something Error !$'
handle		dw	0ffffh  ;句柄
file_seg	dw	0              ;申请内存段地址
file_size	dw 	0,0       ;档大小
max_line	dw	0           ;最大列
cursor_size     dw      0  ;光标大小
line            dw      0  ;列变数,上下可改变
Vmax_line       dw      0  ;最大容许列
Lo_value        dw      0  
Hi_value        dw      0
hex_table       db      '0123456789ABCDEF' ;十六进制表
max_seg         dw      dos_max_seg   ;8  ;实际获得的数,多少64k
line_count      dw      0       ;23   ;屏幕容许的最大列数 = 23 , 减去2列标题

Title1 label byte
db '    <<<<    HEX VIEWER v0.1 (2015)   >>>>         ',24,25,',PgUp,PgDn,Home,End Esc=Quit'
Title2 label byte
db '- Offset -  ----------------'
name_off db '-------------------------------    -- ASCII value --'

key_table       db      up_key,down_key,pgup_key,pgdn_key,home_key,end_key,esc_key ;按键表
no_of_key       equ     $ - offset key_table  ;表长度

process         dw      offset up  ;以下是对应上下等键的子程序进入点
		dw	offset down
                dw      offset pgup
                dw      offset pgdn
                dw      offset home
                dw      offset end_proc
		dw	offset Esc_proc

;-------- code start ------------------
begin:          cld             ;清除方向标志
                mov     ax,cs  ;因为是com, cs和ds,es对齐
                mov     ds,ax
                mov     es,ax
                mov     si,82h ;参数地址第一字元偏移地址
                mov     cl,[si-2]  ;ds=psp  ;取得参数字元个数
		mov 	ch,0  ;清0
                jcxz    help   ;没有输入,离开印出说明
		mov 	dx,si ;档名地址
		add	si,cx ;加长度
		mov 	byte ptr [si-1],0		;清除最末的回车0d
                mov     ax,3d00h     ;开档
		int	21h
Error1:		mov 	dx,offset error_str ;印出错误
		jc	error2  ;失败则离开
		mov 	handle,ax ;存句柄
                mov     di,offset name_off ;指向二列标题的位置
                mov     si,81h ;由参数区81h开始
st10:           lodsb  ;请字元
                or      al,al  ;是否0
                jz      st20  ;是
                mov     ah,al ;存ah
                and     ah,11011111b  ;转大写
                cmp     ah,'A'  ;以下比较是否A-Z
                jb      st15  ;不是
                cmp     ah,'Z'
                ja      st15  ;不是
                mov     al,ah  ;把大写的存al
st15:           stosb           ;存到标题
                jmp     short st10

st20:           mov     al,20h ;最后加一空白
                stosb
                call    cls     ;清屏
                call    clsc    ;绘背景
                mov     ah,3    ;读光标 
                xor     bh,bh
                int     10h
                mov     cursor_size,cx ;存原来光标大小
                call    closecur  ;关闭光标
                call    hex_main       ;主程序
                jmp     short quit ;离开
Help:           mov     dx,offset help_str ;印出说明
Error2:         mov     ah,9
		int	21h
quit:           mov     ah,4ch ;返回dos
                int     21h
;---------------------------------------------------
hex_main:  ;主程序
                mov     bx,offset proc_end  ;指向整个程序末
                mov     sp,bx  ;修改栈顶
                mov     cl,4   
                shr     bx,cl  ; 除16 ,即一段
                inc     bx     ;补1段
                mov     ah,4ah  ;由psp : 0 到 程序末的大小/16,得段大小,用这段来调整可用内存
                int     21H
		jc	hex_err
                mov     bx,dos_max_seg * 1000h                ;8个64k
mem_apply:      mov     ah,48h          ;Allocate Memory  ;申请内存
                int     21h 
                jnc     mem_ok          ;成功则 mem_ok
                sub     bx,1000h        ;失败,则减64k
                dec     max_seg         ;减64k个段
                jnz     mem_apply       ;不等于0,回去再申请
                jmp     short hex_err   ;等于0,则无法申请那怕是一个64k,黯然离开
mem_ok:
                mov     file_seg,ax     ;申请得来的段起点地址
                call    clear_seg       ;请除所得的段内容,全部填上FFFF
                call    read_data       ;读档内容
		jc	hex_err                ;读取错误,离开
                call    print_header    ;印标题

display:        call    display_data    ;印十六进制内容

rekey:          push    cs          ;cs,es对齐
                pop     es
                mov     ah,0            ;读键
		int 	16h
                mov     al,ah           ;存al,即扫瞄码
                mov     cx,no_of_key     ;比较键表长度
		mov	di,offset key_table   ;键表
		repnz	scasb   ;找
                jnz     rekey  ;找不到,回去再读键
		sub	cx,no_of_key  ;减去长度
		neg	cx  ;补
		dec	cx ;调整
		mov	bx,cx  ;得出该键在表中位置
		shl	bx,1   ; x 2 ,因为子程序是字 word 
		mov	si,offset process  ;程序表起点
                call    word ptr cs:[si+bx]  ;指向各键的子程序
                jc      rekey          ;cf=1 则再读新键,即不用刷新资料
                jmp     short display  ;cf=0 刷新资料

hex_err:        stc   ;错误, cf=1
		ret

hex_ok:		clc ;cf=0
		ret
;---------------------------------------------------
clear_seg:      push    es   ;以下是清除段,填上ffff
                mov     bx,file_seg
                mov     bp,max_seg
                mov     ax,-1h   ;ffff

clearSeg10:     mov     es,bx
                mov     di,0
                mov     cx,8000h /2
                rep     stosw
                mov     cx,8000h /2
                rep     stosw
                dec     bp
                jz      clearSeg20
                add     bx,1000h
                jmp     short clearSeg10

clearSeg20:     pop     es
                ret
;---------------------------------------------------
;读档子程序
read_data:	push 	ds
		mov 	bx,handle  ;句柄
		mov 	si,file_seg  ;指向段地址
                mov     ax,max_seg  ;最大64k 个数
                mov     line_count,ax ;暂存在count变数

read10:		mov	ds,si  ;存段
                mov     di,2  ;读2次 8000h ,合共 64k
                mov     dx,0  ;ds:0 存入0 偏移

read15:         mov     cx,8000h  ;64k / 2 
		mov 	ah,3fh  ;读档
		int 	21h
		jc	read_err  ;错
		or 	ax,ax  ;是否0,即读到档末
		jz	read20 ;跳
                cmp     ax,8000h ;是否读到8000h
		jnz	read20  ;不是
                mov     dx,8000h ;指向ds: 8000h,即下半64K
                dec     di  ; 2 次
                jnz     read15 ;读了2次没? 还没,回去再读
                inc     file_size + 2  ;读了64k, 32bit的档大小变数高位加1 ,即 0001:0000
		add	si,1000h	;next segment ;下一64k段
                dec     line_count ;减已申请的64k个数 - 1
                jnz     short read10 ;还未到0,再读

read20:         cmp     di,2  ;是否2
                jz      read27  ;是,则只读了第一次
                add     ax,8000h ;不是,即已读了第一次8000h, ax是最后的大小,加8000h
read27:
                mov     file_size,ax  ;存32bit的档大小变数低位 ,即 0000:xxxx
                mov     dx,file_size + 2 ;取32bit的档大小变数高位
                mov     ax,file_size   ;取32bit的档大小变数低位
		mov 	cx,16   
		div	cx                ;dx:ax 除16
                mov     max_line,ax  ;一列显示16bytes,得最大列数,因为申请最大512k内,除16的结果,一定少65535
                cmp     ax,25 - 2  ; 比较 (屏幕列数-标题)
                ja      read30  ;  ;大于跳
                mov     Vmax_line,0 ;少于,档太小,一屏已显示完,最大列数限制是0,则上下键没反应
                jmp     short read40

read30:         sub     ax,25 - 2 ;列数 -   (屏幕列数-标题)
                inc     ax              ;调整
                mov     Vmax_line,ax  ;列变数的最大限制

read40:         mov     bx,handle  ;取句柄
		mov 	ah,3eh  ;关档
		int	21h
		clc   ;cf = 0
		jmp	short readx

read_err:	stc  ;cf =1

readx:		pop	ds
		ret
;---------------------------------------------------
display_data:   push    es   ;以下是显示十六进制子程序
                push    ds
                mov     line_count,23  ;显示列数
                mov     ax,0b800h       ;指向显示缓冲
                mov     es,ax        ;存ES
                mov     di,(80 * 2)  * 2 ;line 3  ;指向第三列
                mov     ax,space_colAtt ;空白
                stosw
                mov     ax,line     ;列变数

display10:      push    ax          ;保存印出列
                push    di
                push    ax          
                call    print_offset ;印出列变数对应的档偏移,即 00001200 之类
                mov     ax,space_colAtt 
                stosw       ;印出三个空白
                stosw
                stosw                   
                pop     ax  ;回存列
                mov     bx,file_seg ;指向资料段起点
                mov     cx,16 
                mul     cx   ;列 x 16 ,真正档偏移
                ;       dx:ax  ;积在ds:dx
                mov     si,ax  ;段内偏移,存si
display20:
                or      dx,dx   ;是否0
                jz      display30 ;是
                add     bx,1000h ;不是,加64k
                dec     dx  ;减 1
                jmp     short display20 ;回去再试,直到指向正确的64k段

display30:      mov     ds,bx           ;get segment ;段地址
                mov     bp,16           ;1 line 印出16字节

                push    si          ;保存
display40:      lodsb               ;读入字节
                mov     dh,al       ;存dh
                mov     ch,2        ;2次 
                mov     ah,text_color ;色
                call    store_hex       ;output 00 2 chars ;印出dh的ASCII,若dh=FB,印出文字的FB
                mov     ax,space_colAtt  
                stosw       ;一个空白
                dec     bp  ;减1
                jnz     display40 ;未到16再印
                mov     ax,space_colAtt 
                mov     cx,3
                rep     stosw ;三个空白
                pop     si  ;回存si  ;

                mov     cx,16           ;1 line 
                mov     ah,char_color
display50:      lodsb
                stosw           ;印出原来一列的资料
                loop    display50       ;16次
                mov     ax,space_colAtt 
                stosw       ;一空白
                ;       1 line finish  ;完成一列
                pop     di ;回存di
                add     di,80 * 2       ;1 line;下一列
                pop     ax      ;回存列
                inc     ax      ;下一列
                dec     line_count ;是否23列
                jnz     display10 ;不是,回去再列印
                pop     ds
                pop     es
                ret
;---------------------------------------------------
print_offset:   mov     cx,16  ;印出档案偏移子程序
                mul     cx
                ;       dx:ax  ;ax列x16 ,结果在dx:ax
                mov     Lo_value,ax  ;存于变数
                mov     Hi_value,dx
                mov     ah,offset_color ;色
                call    disp_hex  ;印十六进制值
                ret
;---------------------------------------------------
print_header:           ;印出标题子程序
                push    cs
                pop     es 
                xor     dx,dx   ;0,0  ;清0
                mov     bp,offset title1  ;标题1
                mov     cx,80
                mov     bh,0
                mov     bl,title1_color ;色
                mov     ax,1300h
                int     10h  ;印出
                ;
                inc     dh
                mov     bp,offset title2 ;标题2
                mov     cx,80
                mov     bh,0
                mov     bl,title2_color ;色
                mov     ax,1300h
                int     10h
                ret

;---------------------------------------------------
; 上键子程序
up:             cmp     line,0  ;是否0
                jz      upx  ;是,走
                dec     line  ; 减1
                clc
                ret

upx:            stc
                ret
;---------------------------------------------------
 ;下键子程序
down:           mov     ax,line   ;取line
                cmp     ax,vMax_line  ;是否到限制
                jae     downx  ;是,走
                inc     line  ;加列变数
                clc
                ret

downx:          stc
                ret
;---------------------------------------------------
 ;上页键子程序
pgup:           mov     cx,22  ;22次
                mov     bx,cx  ;存

pgup10:         call    up    ;呼叫up
                jc      pgup20  ;是否失败
                loop    pgup10  ;做22次
pgup20:         cmp     cx,bx  ;是否一样
                jz      pgupx  ;一样则这次pgup连一次[上]都做不到,cf=1离开,不做屏幕刷新
                clc   ;cx,bx不一样,即做了一次或以上的up,则cf=0,做屏幕刷新
                ret
pgupx:
                stc
		ret
;---------------------------------------------------
 ;下页键子程序
pgdn:           mov     cx,22  ;22次
                mov     bx,cx  ;存

pgdn10:         call    down  ;呼叫down
                jc      pgdn20 ;是否失败
                loop    pgdn10 ;做22次
pgdn20:         cmp     cx,bx ;是否一样
                jz      pgdnx ;一样则这次pgdn连一次[下]都做不到,cf=1离开,不做屏幕刷新
                clc   ;cx,bx不一样,即做了一次或以上的down,则cf=0,做屏幕刷新
                ret
pgdnx:
                stc
		ret
;---------------------------------------------------
 ;home键子程序
home:           cmp     line,0   ;是否0
                jz      homex  ;是0,离开
                mov     line,0 ;不是0,设line = 0
                clc
                ret

homex:          stc
                ret
;---------------------------------------------------
 ;end键子程序
end_proc:       mov     ax,line  ;取line
                cmp     ax,vMax_line  ;是否到了列限制
                jae     endx  ;大于,走
                mov     ax,vMax_line  ;取列限制
                mov     line,ax ;存line
                clc
                ret

endx:           stc
		ret
;---------------------------------------------------
Esc_proc:       mov     ax,0003h 
                int     10h  ;清屏, 等于cls
                call    quit  ;去dos
		ret
;---------------------------------------------------
;清屏子程序
cls:		mov	ah,0fh                  ;get display mode to al
		int	10h
		mov	ah,0                    ;Set display mode
		int	10h
		ret
;--------------------------------------------------------
;绘背景屏子程序,
clsc:           mov     ax,0600h                 ;cls
                mov     bh,back_color            ;attribute ;蓝
                mov     cx,0                     ;top left
                mov     dl,80
                mov     dh,60
                int     10h
                ret

;------------------------------------------------------
;设定光标子程序
setcur:         push    ax
                push    bx
                mov     ah,2
                xor     bh,bh
                int     10h
                pop     bx
                pop     ax
                ret
;-----------------------------------------------------
;关闭光标子程序
closecur:       pusha
                mov     cx,3030h
                mov     ah,1
                int     10h
                popa
                ret
;-----------------------------------------------------
disp_hex:       ;显示16进制
                mov     dx,Hi_value ;取高位
		mov 	si,2 ;2次 高:低
		; dx = prev_hi			
                ;mov     ah,text_color ;色

change_15:      mov     ch,4  ;4次
                call    store_hex ; 印 0000  
                mov     dx,Lo_value  ;取低位
		dec	si 
		jnz	change_15
		ret
;---------------------------------------------------

; dx,dl      ch may be 2 or 4 for 00 or 0000 ;印出dx或dh十六进制字符,ch=2 or 4

store_hex:      mov     bx,offset hex_table

store_h10:      mov     cl,4
                rol     dx,cl
                mov     al,dl
                and     al,00001111b
                db      2eh     ;CS:
                xlatb
                stosw
                dec     ch
                jnz     store_h10
                ret

;----------------------------------------------------------------------------
proc_end        equ $ + 200h    ;保留給 sp 200h空间
code_seg        ends
                end     start