;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
; 
;
;程序名称:Testreg.asm
;功能:利用子程序印出所有寄存器(范例程式)
;环境:16BIT DOS实模式
;编译器:MASM 5.1-6X
;用法:看说明
;返回值:没有
;破坏寄存器:没有
;
;--------- register_display (Sub program)------------
;原发问人希望改为子程序。
;于是有了这个，可以说是之前X86_Register_display.asm的进阶版。
;改写完了，我发觉这个小玩意也有一点用途。
;它可以实时显示所有寄存器，子程序运行之后，因不改变寄存器值，
;这就相当于一个软中断，放在程式的某些需要监视变数、观看loop值，
;或者查看呼叫中断后传回的值.....等等场合，都能适用。
;为了简便使用，我分别写了两个inc档，Regsub.inc 和DispRegM.inc
;Regsub.inc是子程序，DispRegM.inc是一个宏
;另外写了一个测试用Testreg.asm，就是现在这个。
;
;使用方法：
;(1)纳入inc档,在档顶加入一句：include dispregm.inc
;　因为宏中会使用push 立即数的代码，所以档顶至少要加入.286或以上（如.386）
;
;(2)纳入inc档,在档尾加入一句：include regsub.inc
;
;(3)可在程序中任何位置加入宏，如
;　　@DisplayReg 10,01 　; 立即值，在第10行，印出01(蓝色)的全部寄存器值
;　or @DisplayReg AX,BX ;寄存器赋值，用AX和BX指定的行和颜色
;　or @DisplayReg variable1,variable2 ;变数赋值，用变数指定的行和颜色
;
;
;要注意的是,若要插入@DisplayReg 的程式是驻留式的
;即是使用int21h/ah-31h 或int 27h那种常驻程式,则
;include regsub.inc [必须]放在驻留部份的最底
;-----------
;驻留代码
;
;.驻留的底部...include regsub.inc
;----------
;
;初始化代码
; end
;----------
;
;这是一般驻留程式的结构,被载入后会做一些初始化动作,
;然后[只]把驻留代码留在内存,初始化代码则[丢弃]!
;所以插入的include regsub.inc [必须][必须]
;放在驻留部份的最底,否则.....
;
;还有regsub.inc代码里会自行设定.286的伪代码
;若初始化代码有用到.386,请自行再加上
;就算最顶有.386,也会被regsub.inc的.286取代
;
;
;若不是驻留程式,则include regsub.inc最好放底
;即
;
;.include regsub.inc
;
;CODES ENDS
;END START
;
;
;
;--Code Start----------------------------------------------------------------
.286
;
; TestReg.asm 测试范例档
;
;
include dispregm.inc    ;纳入inc档 -----------------(1)
;----------------------------------------------------------------------------
;	Display All Register  /all registers unchanged
; usage:
;	@DisplayReg 10,01			  ;row and color
;    or @DisplayReg AX,BX			  ;AX=row, BX=color
;    or @DisplayReg variable1,variable2 	  ;row and color variable
;
;----------------------------------------------------------------------------
data segment
        row1            dw      10h ;行变数
        text_color1     dw      4fh ;颜色变数
data ends

STACK SEGMENT STACK
        DB 100 DUP(0)
STACK ENDS

CODES SEGMENT
ASSUME CS:CODES,DS:data,ss:stack

start:          mov     ax,data
                mov     ds,ax
                mov     ax,1111h        ;存入寄存器测试值
                mov     bx,2222h
                mov     cx,3333h
                mov     dx,4444h
                mov     si,5555h
                mov     di,6666h
                mov     bp,7777h
                ;
                stc     ;set cf=1  ;设定cf
                @DisplayReg   ;没有参数，会使用预设----------------------(3)
                ;
                mov     ah,0	       ;暂停
                int     16h
                ;
                ; all flag and registers unchanged after sub-program, except AX(readkey)
                @DisplayReg row1,text_color1  ;variable，变数----------------------(3)
                ;第二次呼叫子程序，除了AX(因为要读键)，所有寄存器和flag不变
                ;
                mov     ah,0     ;暂停
                int     16h

                mov     di,0  ;存入寄存器测试值
                mov     bx,2
                mov     cx,10 ;loop 值
                mov     dx,1000h
                add     si,100

retry:          ; test Register, increase or decrease, and display AX=scan/ascii code
                @DisplayReg 00,4eh  ;immediately,立即值----------------------(3)

                add     di,bx ;改变寄存器值
                add     si,10
                sub     dx,10h
                mov     ah,0
                int     16H   ;读取scan/ascii码放AX,下次DisplayReg印出AX，即扫瞄码
                loop    retry　;cx减值，loop十次，每次改变一些寄存器值
                ;
                mov     ah,4ch ; 离开
                int     21h

;----------------------------------------------------------
include regsub.inc              ;include inc file,纳入inc档 -----------------(2)

CODES ENDS
END START

http://pan.baidu.com/s/1pJvDa23