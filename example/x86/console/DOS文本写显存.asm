;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

;下行定义你的CPU MODEL
.286
data segment
;下行开始定义你的数据
MSG db "hello world!"

data ends

stack segment
;如果入堆栈数据较多，应适当增加BUF中的数值
BUF db 30 dup (?)
TOP EQU THIS WORD
stack ends

code segment
assume ds:data,cs:code
assume ss:stack
start:mov ax,data;初始话数据段段基址
      mov ds,ax;将数据段段基址装入DS
      mov ax,stack;初始化堆栈段段基址
      mov ss,ax;将堆栈段段基址装入SS
      lea sp,TOP;初始化堆栈段减栈定指针SP
;在此插入你的一条指令 ^^
      mov cx,12
      lea di,MSG
      mov dh,10
      mov dl,12
again:mov ah,byte ptr ds:[di]
      call _TEXT_DX
      inc dl
      inc di
      loop again

exit: mov ax,04c00h
      int 021h

;子程序:直接写显存
;入口参数:
;要写的字符=ah(ASCII)
;行=dh
;列=dl
_TEXT_DX proc near
         push ax;保护各种寄存器
         push di
         push ds
         push dx
         push cx

         push ax
         mov ax,0b800h;显存首地址装入DS
         mov ds,ax
         mov ah,dh
         mov al,80
         mul ah
         xor dh,dh
         add ax,dx
         shl ax,1
         mov dx,ax;计算字符偏移地址
         pop ax
         mov di,dx
         mov byte ptr ds:[di],ah

         pop cx
         pop dx
         pop ds
         pop di
         pop ax
         ret
_TEXT_DX endp

code ends
     end start
