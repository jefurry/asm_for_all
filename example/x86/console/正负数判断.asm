;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

; 判断一个数X的正，负数，还是零。（假设是正数，输出+，是负数，输出-，是零，输出This is a zore !）
DATA SEGMENT
X DB 10
CR EQU 0DH
LF EQU 0AH
W DB 'This is a zore!',CR,LF,'$'
ZHENG DB '+',CR,LF,'$'
FU DB '-',CR,LF,'$'
DATA ENDS
CODE SEGMENT
    ASSUME CS:CODE,DS:DATA
START:MOV AX,DATA
    MOV DS,AX
    MOV AL,X
    AND AL,AL
    JZ L1
    SHL AL,1
    JC L3
    JMP L2
L1: MOV DX,OFFSET W
    MOV AH,9
    INT 21H
    JMP L4
L2: MOV DX,OFFSET ZHENG
    MOV AH,9
    INT 21H
    JMP L4
L3: MOV DX,OFFSET FU
    MOV AH,9
    INT 21H
    JMP L4
L4: MOV AH,4CH
    INT 21H
CODE ENDS
    END START