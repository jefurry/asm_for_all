;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

;下行定义你的CPU MODEL
.286
data segment
;下行开始定义你的数据
HEX8 db 07bh
HEX16 dw 0ec76h
BCD_r1 db 2 dup (?)
BCD_r2 db 3 dup (?)

data ends

stack segment stack
;如果入堆栈数据较多，应适当增加BUF中的数值
BUF db 30 dup (?)
TOP EQU THIS WORD
stack ends

code segment
assume ds:data,cs:code
assume ss:stack
start:mov ax,data;初始化数据段段基址
      mov ds,ax;将数据段段基址装入DS
      mov ax,stack;初始化堆栈段段基址
      mov ss,ax;将堆栈段段基址装入SS
      lea sp,TOP;初始化堆栈段减栈定指针SP
;在此插入你的一条指令 ^
begin:lea si,HEX8
      lea di,BCD_r1
      call HEX2BCD8
      lea si,HEX16
      lea di,BCD_r2
      call HEX2BCD16


exit: mov ax,04c00h
      int 021h

;=============================================================
;子程序:转换8位HEX码[8bit]为扩展的BCD码[16bit]
;子程序调用名:HEX2BCD8
;入口参数:ds:[si]=需要转换HEX变量存储地址,08bit
;出口参数:ds:[di]=转换后的BCD变量存储地址,16bit
HEX2BCD8 proc near
;-------------------------------------------------------------
      pusha
      mov al,byte ptr [si]
      xor ah,ah
      mov bl,0ah
      div bl
      mov dl,ah
      xor ah,ah
      div bl
      mov dh,al
      mov al,ah
      xor ah,ah
      mov bl,010h
      mul bl
      add ax,dx
      mov [di],byte ptr ah
      inc di
      mov [di],byte ptr al
      popa
      ret
HEX2BCD8 endp
;=============================================================

;=============================================================
;子程序:转换16位HEX码[8bit]为扩展的BCD码[24bit]
;子程序调用名:HEX2BCD16
;入口参数:ds:[si]=需要转换HEX变量存储地址,16bit
;出口参数:ds:[di]=转换后的BCD变量存储地址,24bit
HEX2BCD16 proc near
;-------------------------------------------------------------
      pusha
      mov ax,[si]
      xor dx,dx
      mov bx,0ah
      div bx
      mov bp,dx
      xor dx,dx
      div bx
      mov cl,04h
      shl dx,cl
      and dx,00f0h
      add bp,dx
      xor dx,dx
      div bx
      mov cl,08h
      shl dx,cl
      and dx,0f00h
      add bp,dx
      xor dx,dx
      div bx
      mov cl,0ch
      shl dx,cl
      and dx,0f000h
      add bp,dx
      xor dx,dx
      div bx
      mov ax,bp
      mov [di],byte ptr dl
      inc di
      mov [di],byte ptr ah
      inc di
      mov [di],byte ptr al
      popa
      ret
HEX2BCD16 endp
;=============================================================

code ends
     end start
