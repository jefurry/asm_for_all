;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

; 设有3个单字节无符号数存放在BUF开始的缓冲区中，编写一个能将它们从大到小从新排列的程序。
DATA SEGMENT
    BUF DB 87,234,123
DATA ENDS
CODE SEGMENT
    ASSUME CS:CODE,DS:DATA
START:MOV AX,DATA
    MOV DS,AX
    MOV SI,OFFSET BUF
    MOV AL,[SI]                ;把3个数取到寄存器中
    MOV BL,[SI+1]
    MOV CL,[SI+2]
    CMP AL,BL                   ;排序，将最大数送AL寄存器
    JAE NEXT1
    XCHG AL,BL
NEXT1:CMP AL,CL
    JAE NEXT2
    XCHG AL,CL
NEXT2:CMP BL,CL                 ;将最小输送CL寄存器
    JAE NEXT3
    XCHG BL,CL
NEXT3:MOV [SI],AL                ;从大到小依次存回缓冲区，AL,BL,CL
    MOV [SI+1],BL
    MOV [SI+2],CL
    MOV AH,4CH
    INT 21H
CODE ENDS
    END START