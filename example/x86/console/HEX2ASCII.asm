;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

DATA SEGMENT
A DB 05FH
DATA ENDS
CODE SEGMENT
     ASSUME CS:CODE,DS:DATA
START:MOV AX,DATA;置DS段初值
      MOV DS,AX
      MOV DL,A
      MOV CL,4
      SHR DL,CL;右移4位获得高4位
      ADD DL,030H;0～9加30H,A~F加37H
      CMP DL,'9';将双分支转化为单分支
      JBE NEXT1
      ADD DL,7
NEXT1:MOV AH,2
      INT 21H;显示高位十六进制
      MOV DL,A
      AND DL,0FH;高4位清0获得低4位
      ADD DL,30H
      CMP DL,'9'
      JBE NEXT2
      ADD DL,7
NEXT2:MOV AH,2
      INT 21
      MOV AH,4CH
      INT 21H
CODE ENDS
     END START
