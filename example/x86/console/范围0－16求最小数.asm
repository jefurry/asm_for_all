;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

; 求十个数中的最小数，并以十进制输出。（若要求最大的，只要把JC 改为JNC 即可）（仅局限于0---16间的数比较,因为ADD AL,30H只是针对一位的十六进制转换十进制的算法）
DATA SEGMENT
XDAT DB 0AH,1FH,02H,03H,04H,05H,06H,07H,08H,09H
MIN DB ?
CR EQU 0DH
LF EQU 0AH
W DB ' is min',CR,LF,'$'
DATA ENDS
CODE SEGMENT
    ASSUME CS:CODE,DS:DATA
START:MOV AX,DATA
    MOV DS,AX
    MOV CX,9
    MOV SI,OFFSET XDAT
    MOV AL,[SI]
L2: CMP AL,[SI+1]
    JC L1
    MOV AL,[SI+1]
L1: INC SI
    LOOP L2
    ADD AL,30H
    MOV DL,AL
    MOV AH,2
    INT 21H
    MOV DX,OFFSET W
    MOV AH,9
    INT 21H
CODE ENDS
    END START