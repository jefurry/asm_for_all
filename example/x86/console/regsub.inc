;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
; 
;副程序名称:regsub.inc
;功能:印出所有寄存器
;环境:16BIT DOS实模式
;编译器:MASM 5.1-6X
;用法:看说明
;返回值:没有
;破坏寄存器:没有
;
; regsub.inc
;这是testreg.asm子程序,请看testreg.asm说明
;
;
.XCREF
.XLIST

;------------------------------------------------------------------------------
;
;usage: push line  ; ex: 0-24 or ax,bx,or word ptr [bx],-1 = current line
;       push color ; ex: 1eh,4fh etc or word ptr [bx], -1 = 07h black/white
;       call reg_sub
;       add sp,4
;       ....
;       ....
;
;
;---------------- REG Display start -------------------------------------------

;.286 disable for new masm
ASSUME DS:nothing,ES:nothing

;----------------------------------------------------------------------------
;    Display All Register  /all registers unchanged
; usage:
;	@DisplayReg 10,01			  ;row and color
;    or @DisplayReg AX,BX			  ;AX=row, BX=color
;    or @DisplayReg variable1,variable2 	  ;row and color variable
;
;
;@DisplayReg     MACRO   UserRow,UserAtt
;                pushf
;                IFNB    <UserRow>
;                push    UserRow
;                ELSE
;                push    00     ;row
;                ENDIF
;                IFNB    <UserAtt>
;                push    UserAtt
;                ELSE
;                push    4Eh    ;color
;                ENDIF
;                call    reg_sub
;                popf            ;ajust sp
;                popf            ;ajust sp
;                popf            ;restore flag, no sp+4 or +6
;
;                ENDM
;
;----------------------------------------------------------------------------

doc_att         equ     07h
reg_att         equ     1eh
mode_add        equ     0b800h
modeX           equ     0
now_page        equ     0

;------------------------------------------------------------------------------
@pushAll        MACRO
                push  ax
                push  bx
                push  cx
                push  dx
                push  si
                push  di
                push  bp
                ENDM

;------------------------------------------------------------------------------
@popAll         MACRO
                pop   bp
                pop   di
                pop   si
                pop   dx
                pop   cx
                pop   bx
                pop   ax
                ENDM
;------------------------------------------------------------------------------

@print_String   MACRO   offst,row,col,att,len,address,write_modeX
                LOCAL ps50              ;direct output to video address

                push    ds
                push    es

                mov     ax,cs
                mov     ds,ax

                push    bp
                mov     bp,address
                mov     es,bp
                mov     cx,len
                lea     si,offst
                xor     ax,ax
                mov     al,row
                shl     ax,1
                shl     ax,1
                add     al,row
                shl     ax,1    ;10
                shl     ax,1    ;20
                shl     ax,1    ;40
                shl     ax,1    ;80
                shl     ax,1    ;160     80 * 2
                shl     dl,1
                add     al,col
                mov     di,ax
                mov     ah,write_modeX
                cmp     ah,1
                mov     ah,att
                jnz     ps50
                add     si,cx
ps50:           lodsb
                stosw
                loop    ps50

                pop     bp
                pop     es
                pop     ds

                ENDM
;------------------------------------------------------------------------------
@display        macro   offst,row,col,att,len,write_modeX,page,write_mode
                LOCAL dis10

                IFNB    <page>
		mov	bh,page
		ELSE
		xor	bh,bh
		ENDIF
                IFNB    <len>
                mov     cx,len
		ELSE
                mov     cx,0ffffh
                lea     di,offst
                xor     al,al
                cld
                repnz   scasb
                neg     cx
                dec     cx
                ENDIF
                IFNB    <col>
                mov     dl,col
                ENDIF
                IFNB    <row>
                mov     dh,row
                ENDIF
                IFNB    <write_mode>
                mov     al,write_mode
		ELSE
                xor     al,al
		ENDIF
                IFNB    <att>
                mov     bl,att
		ELSE
                xor     bl,07h
		ENDIF
                lea     bp,offst
                mov     ah,write_modeX
                cmp     ah,1
                jnz     dis10
                add     bp,cx
dis10:          mov     ah,13h
                int     10h
                endm
;------------------------------------------------------------------------------

reg_sub         proc    near

reg_start:      jmp     reg10

old_ss          dw      0
old_sp          dw      0
old_ax          dw      0
old_ip          dw      0
old_cs          dw      0

sp_flag         db      0
Pre_ax          dw      0
pre_bx          dw      0
pre_cx          dw      0
pre_dx          dw      0

pre_sp          dw      0
pre_bp          dw      0
pre_si          dw      0
pre_di          dw      0

pre_ds          dw      0
pre_es          dw      0
pre_ss          dw      0
pre_cs          dw      0
pre_ip          dw      0
pre_flag        dw      0

xprint_time     db      0
xori_pos        dw      0
user_color      db      0
user_row        db      0,0

line1           db '   AX=      BX=      CX=      DX=      SP=      BP=      SI=      DI=           '
line1_L         equ     $ - offset line1
line2           db '   DS=      ES=      SS=      CS=      IP=                                      '
line2_L         equ     $ - offset line2
flag_0          db      'NV','UP','EI','NT','PL','NZ','NA','PO','NC'
flag_L          equ     $ - offset flag_0
flag_1          db      'OV','DN','DI','TR','NG','ZR','AC','PE','CY'
flag_data	db	27 dup (' ')
flag_data_L	equ	$ - offset flag_data


stack_area      db      400 dup (0)
stack_end       equ     $


reg10:          mov     cs:pre_ax,ax
                mov     cs:pre_bp,bp
                mov     bp,sp
                                ;
                mov     cs:pre_ss,ss
                mov     cs:pre_sp,sp
                mov     ax,cs
                cli
                mov     ss,ax
                mov     sp,offset stack_end
                sti

                @pushAll
                push    ds
                push    es
                pushf

                mov     pre_bx,bx
                mov     pre_cx,cx
                mov     pre_dx,dx
                mov     pre_si,si
                mov     pre_di,di
                mov     ax,es
                mov     pre_es,ax

                pop     ax  ;flag
                mov     pre_flag,ax
                mov     ax,ds
                mov     pre_ds,ax
                mov     ax,cs
                mov     pre_cs,ax
                ;

                mov     ah,03
                int	10h
                mov     xori_pos,dx
                ;
                mov     ax,pre_ss
                mov     ds,ax
                mov     ax,ds:[bp]
                mov     pre_ip,ax
                mov     ax,ds:[bp+2]
                cmp     ax,-1
                jnz     reg20
                mov     al,reg_att
reg20:          mov     user_color,al
                mov     ax,ds:[bp+4]
                cmp     ax,-1
                jnz     reg30
                mov     ah,3
                mov     bh,0
                int     10h
                cmp     dh,22
                jb      reg25
                mov     dh,22
reg25:          mov     al,dh
reg30:          xor     ah,ah
                mov     user_row,al
                inc     al
                mov     user_row+1,al
                ;
                push    cs
                pop     ds
                call    print_head
 
                pop     es
                pop     ds

                mov     bh,0
                mov     dx,xori_pos
                mov     ah,2
                int     10h
                @popAll

                mov     ax,pre_flag
                push    ax
                popf
                mov     bp,pre_bp

                cli
                mov     ax,pre_ss
                mov     ss,ax
                mov     sp,pre_sp
                sti
                mov     ax,pre_ax

                ret

;------------------------------------------------------------------------------
print_head:
                @print_String line1,user_row,0,user_color,line1_L,mode_add,0
                @print_String line2,user_row+1,0,user_color,line2_L,mode_add,0
                ;call    print_foot
                ;mov     dx,0006h   ;first line

                mov     dl,6
                mov     dh,user_row

                mov     bp,8       ;8 regs
                mov     cs:xprint_time,2
                mov     si,offset pre_ax
pd10:           push    dx
                call    xsetcur
                mov     dx,[si]
                call    print_hex
                pop     dx
                add     dl,9
                inc     si
                inc     si
                dec     bp
                jnz     pd10
                dec     cs:xprint_time
                jz      pd20
                mov     bp,5        ;next 5 regs
                ;mov     dx,0106h    ;second line
                mov     dl,6
                mov     dh,user_row+1
                jmp     short pd10
pd20:           call    print_flag
                ret
;------------------------------------------------------------------------------
print_flag:     push    ds
                push    es

                mov     dx,cs
                mov     ds,dx
                mov     es,dx
                
                mov     dx,pre_flag
                mov     si,offset flag_0
                mov     di,offset flag_data
                mov     bx,flag_L
                mov     cl,4
                shl     dx,cl
                mov     cx,6
                mov     bp,1
                call    change_flag1
                mov     cx,3
                mov     bp,0
                call    change_flag
                @display flag_data,user_row+1,48,user_color,flag_data_L,0
                pop     es
                pop     ds
                ret
;------------------------------------------------------------------------------
change_flag:    shl     dx,1
change_flag1:   shl     dx,1
                jc      ch10
                mov     ax,[si]
                jmp     short ch20
ch10:           mov     ax,[si+bx]
ch20:           stosw
                inc     di
                inc     si
                inc     si
                dec     cx
                jz      ch40
                cmp     bp,0
                jnz     change_flag1
                jmp     short change_flag
ch40:           ret
;------------------------------------------------------------------------------
print_hex:      ;word in dx
                mov     ch,4
                jmp     pr5
print_hex1:     mov     ch,2
pr5:            mov     bh,now_page
                mov     bl,doc_att
                mov     ah,0eh
pr10:           mov     cl,4
                rol     dx,cl
                mov     al,dl
                and     al,00001111b   ; char
                add     al,'0'
                cmp     al,'9'
                jbe     pr20
                add     al,7
pr20:           int     10h
                dec     ch
                jnz     pr10
                ret
;------------------------------------------------------------------------------
;cls:    mov ah,0fh                  ;get display mode to al
;    int 10h
;    mov ah,0                    ;Set display mode
;    int 10h
;    ret
;------------------------------------------------------------------------------
xsetcur:        mov     bh,now_page
                mov     ah,2
                int     10h
                ret
;------------------------------------------------------------------------------
reg_sub         endp



.CREF
.LIST
