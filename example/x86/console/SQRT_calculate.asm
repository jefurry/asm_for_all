;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;程序名稱:計算平方根
;功能:輸入一個帶小數的數，計算平方根並輸出
;環境:16BIT DOS實模式
;編譯器:MASM 5.1-6X
;用法:輸入一數（可帶小數），按ENTER


 .MODEL SMALL 
.386 
.387 
.DATA 
 sq_str1 db 'Input a number with decimals (Ex: 1234.5678) : ','$'
 sq_str2 db 0dh,0ah,'Square Root : ','$'
 ten dd 10.0
 temp  dw ?
 sign db ?
 dot_data dd 0
 status dw ?
 integer dd ?
 decimals dd ?

.code  
disps proc near    
        mov ah, 6
        mov dl, al
        int 21h
        ret
disps endp

.startup 
        mov ax,@data
        mov ds,ax
        mov dx,offset sq_str1
        mov ah,9
        int 21h

        fldz ;st=0

p10:
        mov ah,7
        int 21h
        cmp al,0dh
        jz p60
        cmp al,'.'
        jz p20
        mov dl,al
        cmp al,'0'
        jb p10
        cmp al,'9'
        ja p10
        mov ah,2
        int 21h
        ;
        fmul  ten
        mov  ah,0
        sub al, 30h
        mov  temp, ax
        fiadd  temp
        jmp short p10

p20:
        mov dl,al
        mov ah,02
        int 21h ;print '.'
        fld1
p30:
        fdiv  ten
        mov ah,7
        int 21h
        cmp al,0dh      ;enter
        jz p50
        cmp al,'0'
        jb p30
        cmp al,'9'
        ;
        mov dl,al
        mov ah,2
        int 21h
        mov al,dl
        mov  ah, 0
        sub al, 30h
        mov  temp, ax
        fild  temp
        fmul  st, st(1)
        fadd  st(2), st
        fcomp
        jmp short  p30

p50:
        fcomp
p60:
        mov dx,offset sq_str2
        mov ah,9
        int 21h

        ; fld dword ptr dot_data ; load int
         fsqrt ;  get sqrt !
         fstp dot_data ;save

        fstcw status
        or status,0c00h
        fldcw status
        fld dot_data
        ftst
        fstsw ax
        and ax, 4500h
        cmp ax, 0100h
        jnz positive
        mov al, '-'
        call disps
        fabs

positive:
        fld st
        frndint
        fist integer
        fsubr
        fabs
        fstp decimals

        mov eax, integer
        mov ebx, 10
        mov cx, 0
        push bx
again1:
        mov edx, 0
        div ebx
        add dl, 30h
        push dx
        inc cx
        cmp eax, 0
        jnz again1

disp1:
        pop ax
        call disps
        loop disp1
        mov al, '.'
        call disps
        mov eax, decimals
        fstcw status
        xor status, 0c00h
        fldcw status
        fld decimals
        fxtract
        fstp decimals
        fabs
        fistp integer

        mov ecx, integer
        mov eax, decimals
        shl eax, 9
        rcr eax, cl
again2:
        mul ebx
        push eax
        xchg eax, edx
        add al, 30h
        call disps
        pop eax
        cmp eax, 0
        jnz again2

        MOV AH,4CH
        INT 21H

        end

