;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;程序名称:2014fool.asm
;功能:2014年历陽春版
;环境:16BIT DOS实模式(windows/dos或dosbox)
;编译器:MASM 5.1-6X 
;用法:看说明
;返回值:没有
;破坏寄存器:不适用
;
;原题163楼/166楼
;http://tieba.baidu.com/p/3740277941?pn=5
;要求：如果还没学BIOS中断，不用int 10h这些，仅用DOS的int 21h中断该怎么写啊？
;这个[陽春版]以之前的2014easy.asm做蓝本
;应要求，不用int10h，只用3个int21h/ah=7读键，ah=09印字，ah=4ch离开
;
default_week    equ     3
esc_key         equ     1bh
space_key       equ     20h
first_lineY     equ     3
second_lineY    equ     12
first_lineX     equ     2
second_lineX    equ     26
Line_gap        equ     26
m1y             equ     first_lineY
m1x             equ     first_lineX
m2y             equ     first_lineY
m2x             equ     Line_gap + first_lineX
m3y             equ     first_lineY
m3x             equ     Line_gap + Line_gap + first_lineX
m4y             equ     second_lineY
m4x             equ     first_lineX
m5y             equ     second_lineY
m5x             equ     Line_gap + first_lineX
m6y             equ     second_lineY
m6x             equ     Line_gap + Line_gap + first_lineX

        DOSSEG
        .MODEL  small
        .data

month_days      db      31,28,31,30,31,30,31,31,30,31,30,31
screen_buf      db      28 dup (20h),'<<<<   Calendar   >>>>',30 dup (20h)
                db      80 * 22 dup (20h)
                db      21 dup (20h),'Space to Change Half-year, Esc to Exit',21 dup (20h),'$'
month           db      37 dup (0)
curret_month_day dw     0
curret_week     db      default_week  ;2014/1/1 = 3
month_pointer   db      0       ;1
half_year_pointer db    0
Current_last_key db 28
year_head       db      '2014 / '
month_head      db      'September     '
head_L equ      $ - offset year_head
month_L equ     $ - offset month_head
day_buffer      db      head_L * 7  dup (0)
total_L equ     $ - offset year_head
total_Lx equ    $ - offset month_head

week_head db   ' S  M  T  W  T  F  S '

day_str         db ' 1  2  3  4  5  6  7 '
                db ' 8  9 10 11 12 13 14 '
                db '15 16 17 18 19 20 21 '
                db '22 23 24 25 26 27 28 '
                db '29 30 31 '

month_str label byte
        db 'January       ','February      ','March         ','April         '
        db 'May           ','June          ','July          ','August        '
        db 'September     ','October       ','November      ','December      '

posXY label byte
                db      m1x, m1y
                db      m2x, m2y
                db      m3x, m3y
                db      m4x, m4y
                db      m5x, m5y
                db      m6x, m6y
                position_x      db 0
                position_y      db 0
                month_count     dw 0

.stack 200h
.code

start:          mov     ax,@data
                mov     ds,ax
                mov     es,ax
                mov     month_pointer,0
s5:             mov     month_count,0
s10:            call    count_month_day
                mov     di,offset month_head
                mov     cx,total_Lx
                mov     al,' ' ;space
                cld
                rep     stosb
                mov     al,month_pointer
                cbw
                mov     cx,month_L
                mul     cx
                add     ax,offset month_str
                mov     si,ax
                mov     di,offset month_head
                rep     movsb
                mov     si,offset week_head
                mov     di,offset day_buffer
                mov     cx,head_L
                rep     movsb
                ;
                mov     al,curret_week  ;2014/1/1 = 3
                mov     ah,al
                add     al,al
                add     al,ah
                cbw
                add     di,ax
                mov     al,Current_last_key
                mov     ah,0
                mov     cx,ax
                add     cx,cx
                add     cx,ax
                mov     si,offset day_str
                rep     movsb
                mov     cx,8
                mov     si,month_count
                shl     si,1
                add     si,offset posXY
                mov     dx,[si]
                mov     bp,offset year_head
s40:
                push    cx
                call    print_day
                pop     cx
                add     bp,head_L
                inc     dh
                loop    s40
                ;
                inc     month_count
                cmp     month_count,6
                jae     s44
                inc     month_pointer
                jmp     s10

s44:            call    print_scr
s45:            mov     ah,7
                int     21h
                cmp     al,esc_key
                jz      quit
                cmp     al,space_key    ;space
                jnz     s45
                xor     half_year_pointer,1  ;toggle开关
                test    half_year_pointer,1 ;是否0
                mov     month_pointer,0 ;;先设0
                jz      s52 ;是0则跳,表示month_pointer=0 (上半年)
                mov     month_pointer,6 ;不是0则表示month_pointer= 6(下半年)
s52:            jmp     s5
quit:
                mov     ah,4ch
                int     21h
;-------------------------------------------------------------
print_day:      mov     cx,head_L
                mov     al,dh
                mov     ah,0
                mov     bx,80
                push    dx
                mul     bx
                pop     dx
                mov     bl,dl
                add     ax,bx
                mov     di,offset screen_buf
                add     di,ax
                mov     si,bp
                rep     movsb
                ret
;--------------------------------------------------------
print_scr:      mov     dx,offset screen_buf
    	        mov	ah,9
		int	21h
                ret
;---------------------------------------------------------------------------
;month_days      db      31,28,31,30,31,30,31,31,30,31,30,31

count_month_day:
                cmp     month_pointer,0
                jnz     cm20
                mov     curret_week,default_week ;3
		mov	al,month_pointer
                mov     ah,0
                mov     bx,offset month_days
                xlatb
                mov     Current_last_key,al
		ret

cm20:           mov     al,month_pointer
                mov     bx,offset month_days
                xlatb
                mov     Current_last_key,al
                mov     al,month_pointer
                dec     al
                xlatb
                mov     ah,0
                sub     al,28
                add     al,curret_week
                cmp     al,6
                jbe     cm40
                sub     al,7
cm40:           mov     curret_week,al
                ret
;---------------------------------------------------------------------------
                end     start