;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;这是一个印出档案资料最最基本的程式，它先建立一个DTA
;再呼叫int21h / ah=4eh找出档案的资料，然后依次印出
;档名，档案大小，最後修改日期和最後修改时间
;若给出档名有万用字元如* 和？,再用int21h / ah=4fh ,第二个找到的
;档案可再在DTA抓出资料，如此类推，这相当于部份dir的功能
;
;
data segment

buffer db 80H dup(?)
filename db 'E:\test',0

name_list    	db	14 dup (' ')
file_data		db	 '       '
size_end		db	 ' '
			db	 ' '		
file_month		dw	0
			db	'-'
file_date		dw	0
			db	'-'
file_year		dw	0
			dw	0
			db	' '
file_hour		dw	0
			db	 ':'
file_min		dw	0,'$'
zero_sign		db	 '           0'
zero_L                  equ     $ - offset zero_sign

data ends

stack segment
                db 50 dup(?)
stack ends

code segment
assume cs:code,ds:data,ss:stack
p proc far
                mov     ax,data
                mov     ds,ax
                mov     es,ax
                cld
                mov     dx,offset buffer
                mov     ah,1ah
                int     21h
                lea     dx,filename
                mov     cx,0ffffh
                mov     ah,4eh
                int     21h
		jnc 	pr_0
		jmp	quit

pr_0:
                mov     si,offset buffer +15h

                mov     di,offset name_list
                mov     si,offset buffer
                add     si,1eh
                mov     cx,15
                push    si

pr_1:           lodsb
                or      al,al
                jz      pr_2
                stosb
                loop    pr_1

pr_2:           sub     cx,15
                neg     cx

pr_3:		mov	al,' '
		rep	stosb
		mov 	si,offset buffer

		mov	ax,ds:[si+1ah]                   ;size lo
		mov	dx,ds:[si+1ch]                   ;size hi
		or	dx,dx
		jnz	pr_40
		or	ax,ax
		jnz	pr_40

pr_30:		mov	bx,offset zero_sign             ;size = 0

pr_35:		push	si
		push	di
		mov	si,bx
		mov	di,offset name_list + zero_L
		mov	cx,zero_L
		rep	movsb
		pop	di
		pop	si
		jmp	short pr_50

pr_40:		mov	di,offset size_end
		mov	cx,10
		call	store_dec

pr_50:		mov	dx,word ptr [si+18h]
		mov	ax,dx
		and	al,00011111b
		aam
		or	ax,3030h
		xchg	ah,al
		mov	file_date,ax
		mov	ax,dx
		mov	cx,5
		shr	ax,cl
		and	al,00001111b            ;get month
		aam
		or	ax,3030h
		xchg	ah,al
		mov	file_month,ax

		mov	ax,dx
		mov	cx,9
		shr	ax,cl
		add	ax,1980		;first Year
		
		push	di
		push	si
		push	bx
		push	dx
		mov	di,offset file_year
		mov	si,10
		mov	bl,30h
		xor	cx,cx
pr_55:		xor	dx,dx
		div	si
		or	dl,bl
		push	dx
		inc	cx
		or	ax,ax
		jnz	pr_55

pr_60:		pop	ax
		stosb
		loop	pr_60
		;
		pop	dx
		pop	bx
		pop	si
		pop	di
		;
pr65:		
		mov	dx,word ptr [si+16h]     ;hour ,minute, second
		mov	ax,dx                   ;xxxxx xxxxxx xxxxx
		mov	cx,11
		shr	ax,cl
		and	al,00011111b

		aam
		or	ax,3030h
		xchg	ah,al
		mov	file_hour,ax
		mov	ax,dx
		mov	cx,5
		shr	ax,cl
		and	al,00111111b
		aam
		or	ax,3030h
		xchg	ah,al
		mov	file_min,ax
		
		mov 	dx,offset name_list
		mov	ah,9
		int	21h

quit:
		mov 	ah,4ch
		int	21h

p endp

;-----------------------------------------------------------
.286
;input  :       dx-high ax-low
;   cx  :       Max num of digit
;es:di  :       offset of last byte of digit
;output :       Output to string array
store_dec: 	pusha
		std
		xchg	bp,dx
		mov	si,10                 ;div by 10
		mov	bx,30h
store_dec1:		or	bp,bp
		jz	store_dec3
		xchg	bp,ax
		xor	dx,dx
		div	si
		xchg	bp,ax
		div	si
		or	dl,bl
		push	ax
		mov	al,dl
		stosb
		dec	cx
		pop	ax
		jmp	store_dec1

store_dec3:		xor	dx,dx
		div	si
		or	dl,bl
		push	ax
		mov	al,dl
		stosb
		dec	cx
		pop	ax
		or	ax,ax
		jnz	store_dec3
		jcxz	store_dec4
		mov	al,' '
		rep	stosb

store_dec4:	cld
		popa
		ret
;--------------------------------------
code ends
end p

