;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

; 设某一数组的长度为N个元素,均为字数据，试编制一个程序使该数组中的数据按照从小到大的次序排列。（要从大到小排序，只需把JBE改为JAE即可）
ADATA SEGMENT
    N EQU 10
    DATA DW 87,54,78,45,35,23,04H,12H,03H,21H
    ADATA ENDS
ACODE SEGMENT
    ASSUME CS:ACODE,DS:ADATA
    START:MOV AX,ADATA
    MOV DS,AX
    MOV DS,AX
    MOV BX,0
    MOV CX,N
    DEC CX
    LOP1: MOV DX,CX
    LOP2: MOV AX,DATA[BX]
    CMP AX,DATA[BX+2]
    JBE CONTI
    XCHG AX,DATA[BX+2]
    MOV DATA[BX],AX
    CONTI: ADD BX,2
    LOOP LOP2
    MOV CX,DX
    MOV BX,0
    LOOP LOP1
    MOV AH,4CH
    INT 21H
    ACODE ENDS
END START