;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

;下行定义你的CPU MODEL
.286
data segment
;下行开始定义你的数据
_ASC1 db "12345"
_ASC2 db "012345"
_ASC3 db "7"
_ASC4 db "06"
_N1 db 3 dup (?)
_N2 db 3 dup (?)
_N3 db ?
_N4 db ?
data ends

stack segment
;如果入堆栈数据较多，应适当增加BUF中的数值
BUF db 30 dup (?)
TOP EQU THIS WORD
stack ends

code segment
assume ds:data,cs:code
assume ss:stack,es:data
start:mov ax,data;初始话数据段段基址
      mov ds,ax;将数据段段基址装入DS
      mov eS,ax
      mov ax,stack;初始化堆栈段段基址
      mov ss,ax;将堆栈段段基址装入SS
      lea sp,TOP;初始化堆栈段减栈定指针SP
;在此插入你的一条指令 ^^
lea si,_ASC1
lea di,_N1
mov cx,5
call _ASCII2BCD

lea si,_ASC2
lea di,_N2
mov cx,6
call _ASCII2BCD

lea si,_ASC3
lea di,_N3
mov cx,1
call _ASCII2BCD

lea si,_ASC4
lea di,_N4
mov cx,2
call _ASCII2BCD





exit: mov ax,04c00h
      int 021h

;子程序:ASCII数字字符转BCD码
;入口参数 ES:SI=源地址,DS:DI=目的地址,CX=转换的字数

_ASCII2BCD proc near
           push cx;保护CX
 A2B_AGAIN:and byte ptr es:[si],0fh;将ES:[SI]内存中的ASCII码数字转换为非压缩BCD码
           inc si
           loop A2B_AGAIN
           pop cx;恢复CX 
           cmp cx,1
           je _BCD8421
           push cx;CX再次入栈保护
           shr cx,1
           adc cx,0
           dec cx
           add di,cx
           pop cx
  _BCD8421:dec si
           mov al,es:[si];将待压缩的低位装入AL
           cmp cx,1
           jne NEXT_0005
           dec cx
           mov ah,00h
           xor cx,cx
           jmp next_0006
 NEXT_0005:dec cx
           dec si
           mov ah,es:[si];将待压缩的高位装入AL
           shl ah,4
           add ah,al
           mov DS:[DI],ah
           dec di
           loop _BCD8421
           ret
 next_0006:add ah,al
           mov DS:[DI],ah
           ret
_ASCII2BCD endp


code ends
     end start

