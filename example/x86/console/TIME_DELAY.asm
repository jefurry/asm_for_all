;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;副程序名稱:delay_proc
;功能:時間延遲
;環境:16BIT DOS實模式
;編譯器:MASM 5.1-6X
;用法:把延遲秒數放入AX,呼叫delay_proc
;返回值:沒有
;破壞寄存器:沒有
;備註:若AX=0,即時返回
;
delay_proc proc near
    push es
	push dx
	or ax,ax ;is it zero?
	jz delayx
	mov dx,40h
	mov es,dx
	mov dx,ax
	add dx,ax ; x 2
	shl ax,1  ;x 2
	shl ax,1  ;x 4
	shl ax,1  ;x 8
	shl ax,1  ;x 16
        add ax,dx ; 16 + 2 = 18
	mov dx,es:[006ch] ;get system time count
	add dx,ax ; set limit 
delayLoop:
	cmp es:[006ch],dx ;reach ?
	jbe delayLoop  ;no, check again
delayx:	pop dx
	pop es
	ret

delay_proc endp 