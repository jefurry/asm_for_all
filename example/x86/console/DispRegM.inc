;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
; 
; DispRegM.inc
;这是testreg.asm子程序,请看testreg.asm说明
;
;
;副程序名称:dispregM.inc
;功能:配合regsub.inc的宏
;环境:16BIT DOS实模式
;编译器:MASM 5.1-6X
;用法:看说明
;返回值:没有
;破坏寄存器:没有
;
.XCREF
.XLIST
;----------------------------------------------------------------------------
;    Display All Register  /all registers unchanged
; usage:
;	@DisplayReg 10,01			  ;row and color
;    or @DisplayReg AX,BX			  ;AX=row, BX=color
;    or @DisplayReg variable1,variable2 	  ;row and color variable
;
@DisplayReg     MACRO   UserRow,UserAtt
                pushf
                IFNB    <UserRow>
                push    UserRow
                ELSE
                push    00     ;row
                ENDIF
                IFNB    <UserAtt>
                push    UserAtt
                ELSE
                push    4Eh    ;color
                ENDIF
                call    reg_sub
                popf            ;ajust sp
                popf            ;ajust sp
                popf            ;restore flag, no sp+4 or +6

                ENDM
;----------------------------------------------------------------------------
.CREF
.LIST