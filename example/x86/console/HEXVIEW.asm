;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

.286
;===================================================================
;数据定义:
;HEXTAB:ASCII转换表
;TARS_HEX:将要转换为ASCII码的2字节十六进制码
;HEX_PRINT:要输出的说明内容
;===================================================================

data segment
HEXTAB db "0123456789ABCDEF"
TARS_HEX dw 04fc8h
HEX_PRINT db "HEX TO ASCII Program(Algorithm:XLAT)",0dh,0ah
MSG_LINE_2 db "Write by Bokoisme(Ziyi.Xiang)",0dh,0ah
MSG_LINE_3 db "November-20,2011",0dh,0ah
MSG_LINE_4 db "Data in memory address:"
_DATA_SEG db 4 dup (?)
L4P2 db ":["
_DATA_PON db 4 dup (?)
L4P3 db "]",0dh,0ah
MSG_LINE_5 db "Transform HEX data:(0x04fc8h)",0dh,0ah
MSG_LINE_6 db "Transform ASCII result:0"
_HEX db 4 dup (?)
L6P2 db 'H',0dh,0ah
_char_end db "Pleas confirm transform result",0dh,0ah,'$'
RECODE db 00
data ends

;===================================================================
;堆栈段 堆栈容量30
;===================================================================

stack segment
_BUFF db 30 dup (?)
TOP EQU THIS WORD
stack ends

;===================================================================
;这是一个将内存中字符转换为ASCII码并输出到屏幕的程序
;本代码使用查表法作为算法思路
;Writer by Bokoisme(Xiang)
;===================================================================

code segment
     assume cs:code,ds:data
     assume ss:stack
start:mov ax,data;初始话数据段段基址
      mov ds,ax;将数据段段基址装入DS
      mov ax,stack;初始化堆栈段段基址
      mov ss,ax;将堆栈段段基址装入SS
      lea sp,TOP;初始化堆栈段减栈定指针SP
      mov DX,TARS_HEX;将带转换的数据装入子程序入口参数
      lea di,_HEX;将转换结果地址装入DI
      call HEX2ASCII16;调用转换函数HEX2ASCII
      mov dx,ds
      lea di,_DATA_SEG
      call HEX2ASCII16
      lea dx,TARS_HEX
      lea di,_DATA_PON
      call HEX2ASCII16
      lea dx,HEX_PRINT;将输出结果字符串地址首地址装入入口参数DX,输出地址尾部以ASCII代码$结束
      call _PRINT;调用输出函数_PRINT
 exit:mov ah,04ch
      int 021h

;===================================================================
;子程序:HEX2ASCII16
;功能:将2字节的十六进制数转换为4字节ASCII码
;入口参数:DX 待转换的十六进制数,DS:DI 转换ASCII码后存储段基址:偏移量
;出口参数:将转换后的数据存入 DS:DI的内存中
;保护所有寄存器
;子程序:HEX2ASCII8
;功能:将1字节十六进制数转换为2字节ASCII码
;入口参数:al 待转换的十六进制数
;出口参数:ah为转换后的ASCII高位 al为转换后的ASCII低位
;保护寄存器BX,CX,DX
;===================================================================

HEX2ASCII8 proc near
H2A8_start:push bx
           push cx
           push dx
           lea bx,HEXTAB
           mov dl,al
           mov cl,4
           shr al,cl
           XLAT
           mov ah,al
           mov al,dl
           and al,0fh
           XLAT
           pop dx
           pop cx
           pop bx
 H2A8_exit:ret
HEX2ASCII8 endp

HEX2ASCII16 proc near
H2A16_start:pusha
            mov al,dh
            call HEX2ASCII8
            mov byte ptr ds:[di],ah
            inc di
            mov byte ptr ds:[di],al
            inc di
            mov al,dl
            call HEX2ASCII8
            mov byte ptr ds:[di],ah
            inc di
            mov byte ptr ds:[di],al
            popa
 H2A16_exit:ret
HEX2ASCII16 endp

;===================================================================
;子程序:_PRINTF
;功能:调用DOS 21h #09功能输出DS:DX中的字符串，字符串以$结尾
;入口参数:DS:DX
;出口参数:无
;===================================================================

_PRINT proc near
print_start:mov ah,09h
            int 021h
 print_exit:ret
_PRINT endp

code ends
end start

