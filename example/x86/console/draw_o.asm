;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.

;This file draw a circle in the middle of screen
;the circle can be big than screen,move core position (x,y),change color
;compile use Miniasm
;Date & Time:    11:39,30th,july,2014


org 256	;disable out dos format


;code segment
;keep outer environment
push    ds

push	ax
push	bx
push	cx
push	dx
push	si
push	di

; also you can add for pushing di si sp bp

;=====   code body    =====
;------------------------------------------------

;open svga mode
mov	ax,	4f02h
mov	bx,	0105h
int	10h

;test draw a line
mov	dx,	0
mov	bh,	0

next_line:
mov	cx,	0
next_pixel:
call	func_round1

inc	cx
cmp	cx,	word [XMAX]
jbe	byte next_pixel

inc	dx
cmp	dx,	word [YMAX]
jbe	byte next_line



;------------------------------------------------
;=====  code body end =====



;restore outer environment
pop	di
pop	si
pop	dx
pop	cx
pop	bx
pop	ax

pop     ds


jmp	word finish

; -- function segment --
;-------------------------

func_round1:
;inport:(cx,dx)

push	ax
push	bx
push	di
push	si

;check draw area
xor	si,	si
xor	di,	di

mov	ax,	word [CENTER_X]
sub	ax,	word [R]	;CENTER_X - R
jc	byte	ck_round1a
cmp	cx,	ax
jb	byte	goout_round1

ck_round1a:
mov	ax,	word [CENTER_X]
add	ax,	word [R]	;CENTER_X + R
jc	byte	ck_round1b
cmp	cx,	ax
jg	byte	goout_round1

ck_round1b:
mov	ax,	word [CENTER_Y]
sub	ax,	word [R]	;CENTER_Y - R
jc	byte	ck_round1c
cmp	dx,	ax
jb	byte	goout_round1

ck_round1c:
mov	ax,	word [CENTER_Y]
add	ax,	word [R]	;CENTER_Y + R
jc	byte	ck_round1d
cmp	dx,	ax
jg	byte	goout_round1

ck_round1d:	;go on
cmp	cx,	word [CENTER_X]
jb	byte ck_round1_34

jmp	byte goon_round1

;--------------
goout_round1:
jmp	word out_round1
;--------------

goon_round1:
;check round1 12
;cx >= center x
cmp	dx,	word [CENTER_Y]
jb	byte round1_area1
jmp	byte round1_area2

ck_round1_34:
;cx < center x
cmp	dx,	word [CENTER_Y]
jb	byte round1_area4
jmp	byte round1_area3

round1_area1:
;cx >= center x
;dx < center y
mov	ax,	word [CENTER_X]
mov	bx,	cx
sub	bx,	ax
mov	si,	bx

mov	ax,	word [CENTER_Y]
mov	bx,	dx
sub	ax,	bx
mov	di,	ax

jmp	word	round1_count_p

round1_area2:
;cx >= center x
;dx >= center y
mov	ax,	word [CENTER_X]
mov	bx,	cx
sub	bx,	ax
mov	si,	bx

mov	ax,	word [CENTER_Y]
mov	bx,	dx
sub	bx,	ax
mov	di,	bx

jmp	word	round1_count_p

round1_area3:
;cx < center x
;dx >= center y
mov	ax,	word [CENTER_X]
mov	bx,	cx
sub	ax,	bx
mov	si,	ax

mov	ax,	word [CENTER_Y]
mov	bx,	dx
sub	bx,	ax
mov	di,	bx

jmp	word	round1_count_p

round1_area4:
;cx < center x
;dx < center y
mov	ax,	word [CENTER_X]
mov	bx,	cx
sub	ax,	bx
mov	si,	ax

mov	ax,	word [CENTER_Y]
mov	bx,	dx
sub	ax,	bx
mov	di,	ax

round1_count_p:
push	dx
push	cx
push	bx


mov	ax,	si
mul	si
mov	bx,	dx	;keep high byte in bx
mov	si,	ax

mov	ax,	di
mul	di
mov	cx,	dx	;keep high byte in cx
mov	di,	ax

clc
add	si,	di
adc	bx,	cx	;add high byte
mov	di,	bx

xor	dx,	dx
mov	ax,	word [R]
mul	ax

clc
cmp	di,	dx

pop	bx
pop	cx
pop	dx

jb	byte	drawp_round1
je	byte	cmp_low_round1
jmp	word	out_round1

cmp_low_round1:
clc
cmp	ax,	si

;compare r^2 ? x^2+y^2
jb	byte	out_round1	;(cx,dx) out of circle area

;draw point
drawp_round1:
mov	ah,	0ch
mov	al,	byte [CIRCLE_COLOR]	;draw color
mov	bh,	0
int	10h

;finish_round1

out_round1:
pop	si
pop	di
pop	bx
pop	ax

ret

;-------------------------


; -- data segment --
;-------------------------
XMAX		dw		1023	;screen max of x
YMAX		dw		767	;screen max of y

CENTER_X	dw	500	;(x, )
CENTER_Y	dw	350	;( ,y)
R		dw	110	;circle radius

CIRCLE_COLOR	db	20h

;-------------------------


finish:

;return to DOS 
  mov ax,4c00h 
  int 21h